"   Author: Paweł Wojaczek
"   https://gitlab.com/pwojx

"           _         
"    _   __(_)___ ___ 
"   | | / / / __ `__ \
"   | |/ / / / / / / /
"   |___/_/_/ /_/ /_/ 
"

" === leader key ===
let mapleader=" "

" === general ===
syntax on
set confirm
set hlsearch
set incsearch
set noerrorbells
set novisualbell
set shiftwidth=4
set softtabstop=4
set expandtab
set smarttab
set autoindent
set smartindent
set cursorline
set number relativenumber
set nowrap
set ignorecase
set smartcase
set noswapfile
set nobackup
set undodir=~/.vim/undodir/
set undofile
set encoding=utf-8
set fileencoding=utf-8
set mouse=a
set splitbelow
set splitright
set noshowmode "only if lightline is installed

" === vim-plug ===
call plug#begin()
Plug 'morhetz/gruvbox'
Plug 'cocopon/iceberg.vim'
Plug 'nanotech/jellybeans.vim'
Plug 'ayu-theme/ayu-vim'
Plug 'preservim/nerdtree'
Plug 'Raimondi/delimitMate'
Plug 'chrisbra/unicode.vim'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'itchyny/lightline.vim'
call plug#end()

" === colorscheme ===
set termguicolors
set background=dark
let ayucolor="light"
colorscheme jellybeans

" === NERDTree ===
nnoremap <Leader>f :NERDTreeToggle<CR>
nnoremap <silent> <Leader>v :NERDTreeFind<CR>
let NERDTreeShowBookmarks=1
let NERDTreeShowHidden=1

" === Unicode.vim ===
nnoremap <Leader>us :UnicodeSearch<Space>

" === Conquer of Completion (Coc) ===
source ~/.config/nvim/coc.vim

" === key bindings ===
" edit .vimrc
nnoremap <Leader>rc :e ~/.config/nvim/init.vim<CR>
" edit i3 config file
nnoremap <Leader>i3 :e ~/.config/i3/config<CR>
" move cursor by display lines when wrapping
noremap <silent> j gj
noremap <silent> k gk
noremap <silent> 0 g0
noremap <silent> $ g$
" Y yanks to the end
nnoremap Y y$
" using system clipboard
noremap <Leader>y "+y
noremap <Leader>Y "+y$
noremap <Leader>d "+d
noremap <Leader>D "+D
noremap <Leader>p "+p
noremap <Leader>P "+P
" vertical resizing
nnoremap <silent> <Leader>= :vertical resize +5<CR>
nnoremap <silent> <Leader>+ :vertical resize +5<CR>
nnoremap <silent> <Leader>- :vertical resize -5<CR>
" expanding {} blocks
inoremap {<CR> {<CR>}<Esc>O
inoremap {;<CR> {<CR>};<Esc>O
" turn off highlight on screen reload
nnoremap <C-l> :nohl<CR><C-l>
" quick find and replace
nnoremap <Leader>s :%s//g<Left><Left>
nnoremap <Leader>S :%s//gc<Left><Left><Left>
" moving highlighted lines up and down
vnoremap J :m '>+1<CR>gv=gv
vnoremap K :m '<-2<CR>gv=gv
" switching between splits
map <Leader>h :wincmd h<CR>
map <Leader>j :wincmd j<CR>
map <Leader>k :wincmd k<CR>
map <Leader>l :wincmd l<CR>
" toggle word wrap
map <Leader>ww :set wrap!<CR>
" search for highlighted text
vnoremap // y/\V<C-R>=escape(@",'/\')<CR><CR>
