#   Author: Paweł Wojaczek
#   https://gitlab.com/pwojx

# Include .bashrc if it exists and if running bash.
[ -n "$BASH_VERSION" ] && [ -f "$HOME/.bashrc" ] && . "$HOME/.bashrc"

# Set PATH so it includes user's private bin if it exists.
[ -d "$HOME/bin" ] && PATH="$HOME/bin:$PATH"
[ -d "$HOME/.local/bin" ] && PATH="$HOME/.local/bin:$PATH"

# Start i3 if user logs into tty1.
if [[ "$(tty)" = "/dev/tty1" ]]; then
    pgrep i3 || startx
fi
