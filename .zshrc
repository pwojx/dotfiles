#   Author: Paweł Wojaczek
#   https://gitlab.com/pwojx

#                __  
#    ____  _____/ /_ 
#   /_  / / ___/ __ \
#    / /_(__  ) / / /
#   /___/____/_/ /_/ 
#

# Prompt.
setopt prompt_subst
PS1='%B%F{green}%n@%M%f:%F{blue}%~%f $(git_info)%(!.#.»)%b '
#PS1='%B%F{red}[%f%F{yellow}%n%f%F{green}@%f%F{blue}%M%f %F{magenta}%~%f%F{red}]%f%b%(!.#.$) '
PS2='\ '
RPS1='%(?..%F{red}%? ↵%f)'

# History.
HISTFILE=~/.histfile
HISTSIZE=2000
SAVEHIST=2000
setopt EXTENDED_HISTORY
setopt INC_APPEND_HISTORY
setopt HIST_IGNORE_DUPS

# Aliases.
[ -f ~/.aliases ] && . ~/.aliases

# Key bindings.
bindkey -v
bindkey "^A" beginning-of-line
bindkey "^E" end-of-line
bindkey "^K" kill-line
bindkey "^R" history-incremental-search-backward
bindkey '^ ' autosuggest-accept     # Ctrl + space
bindkey '^[^M' autosuggest-execute  # Alt + enter

# Autocompletion.
autoload -Uz compinit
compinit
zstyle ':completion:*' menu select
zstyle ':completion:*' matcher-list '' 'm:{a-z}={A-Z}' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=* l:|=*'

# Use vim keys in Tab completion menu.
zmodload zsh/complist
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'j' vi-down-line-or-history
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey '^?' backward-delete-char

# Colored man pages.
function man() {
    env \
        LESS_TERMCAP_md=$(tput bold; tput setaf 4) \
        LESS_TERMCAP_me=$(tput sgr0) \
        LESS_TERMCAP_mb=$(tput blink) \
        LESS_TERMCAP_us=$(tput setaf 2) \
        LESS_TERMCAP_ue=$(tput sgr0) \
        LESS_TERMCAP_so=$(tput smso) \
        LESS_TERMCAP_se=$(tput rmso) \
        PAGER="${commands[less]:-$PAGER}" \
        man "$@"
}

# Plugin: zsh-autosuggestions.
if [ -f $HOME/.zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh ]; then
    source $HOME/.zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
    ZSH_AUTOSUGGEST_STRATEGY=(history completion)
    ZSH_AUTOSUGGEST_HISTORY_IGNORE='cd *'
    ZSH_AUTOSUGGEST_COMPLETION_IGNORE='sudo apt install *'
fi

# Plugin: zsh-syntax-highlighting.
if [ -f $HOME/.zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh ]; then
    source $HOME/.zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
    ZSH_HIGHLIGHT_STYLES[suffix-alias]=fg=cyan,underline
    ZSH_HIGHLIGHT_STYLES[precommand]=fg=cyan,underline
    ZSH_HIGHLIGHT_STYLES[arg0]=fg=cyan
    ZSH_HIGHLIGHT_HIGHLIGHTERS=(main brackets pattern)
    ZSH_HIGHLIGHT_PATTERNS=('rm -rf *' 'fg=white,bold,bg=red')
fi

# Plugin: zsh-git-prompt.
if [ -f $HOME/.zsh/plugins/zsh-git-prompt/zshrc.sh ]; then
    source $HOME/.zsh/plugins/zsh-git-prompt/zshrc.sh
    ZSH_THEME_GIT_PROMPT_PREFIX='git:('
    ZSH_THEME_GIT_PROMPT_SUFFIX=')'
fi
if [ -f $HOME/.zsh/plugins/git.zsh ]; then
    source $HOME/.zsh/plugins/git.zsh
fi
