#   Author: Paweł Wojaczek
#   https://gitlab.com/pwojx

# Wrapper function for a `git` command.
# It is used in order to avoid interfering with manually-run git commands by the user.
# Equivalent to `git --no-optional-locks` but has wider compatibility with older versions of git.
function __secure_git() {
    GIT_OPTIONAL_LOCKS=0 command git "$@"
}

# Function which determines if a directory is under a git control.
function git_prompt() {
    [[ $(__secure_git rev-parse --is-inside-git-dir 2> /dev/null) = "true" ]] || \
    [[ $(__secure_git rev-parse --is-inside-work-tree 2> /dev/null) = "true" ]] || \
    echo "false"

    echo "true"
}

# Function used to display git information only inside directories which are under a git control.
# zsh-git-prompt plugin is required for this function to work.
function git_info() {
    [[ $(git_prompt) = "true" ]] && echo $(git_super_status) ""
}
